using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kinect = Windows.Kinect;

public class Visualizer_FadeIn : MonoBehaviour
{
    public Material boneMaterial;
    public Material transparentMat;
    public BodySourceView bsv;

    public bool wristLeftTriggered;
    public bool wristRightTriggered;

    private JointsData data;
    private Dictionary<Kinect.JointType, Kinect.JointType> _BoneMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
    {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },

        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },

        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
        { Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.WristRight },
        { Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
    };

    private float t;
    private List<GameObject> jointObjs = new List<GameObject>();
    private bool beginUpdate;
    private GameObject body;

    void Update()
    {
        if (!beginUpdate)
            return;
        
        if (t >= 1)
        {
            Destroy(body);
            Destroy(gameObject);
            return;
        }

        t += Time.deltaTime / 2;
        Color newColor = new Color(1, 1, 1, Mathf.Lerp(0, 1, t));

        foreach(GameObject go in jointObjs)
        {
            go.GetComponent<MeshRenderer>().material.color = newColor;
            go.GetComponent<LineRenderer>().startColor = newColor;
            go.GetComponent<LineRenderer>().endColor = newColor;
        }

        // After half time, report if player WristLeft or WristRight late or not
        if (t > 0.5f)
        {
            if (!wristLeftTriggered)
            {
                bsv.wristLeftLate = true;
            }
            if (!wristRightTriggered)
            {
                bsv.wristRightLate = true;
            }
        }
    }

    public void SetData(JointsData data)
    {
        this.data = data;
    }

    public void ShowBody()
    {
        // Create GameObject body
        body = new GameObject("Recorded Body (Visualizer_FadeIn)");
        body.transform.parent = gameObject.transform;

        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            // Skip these joints
            if (jt == Kinect.JointType.Head || jt == Kinect.JointType.ThumbLeft || jt == Kinect.JointType.ThumbRight
                    || jt == Kinect.JointType.HandLeft || jt == Kinect.JointType.HandRight
                    || jt == Kinect.JointType.HandTipLeft || jt == Kinect.JointType.HandTipRight)
                continue;

            // Create GameObject cubes for joints
            GameObject jointObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
            jointObj.GetComponent<MeshRenderer>().material = transparentMat;

            // Give BoxCollider.isTrigger true and TriggerDetector to WristLeft and WristRight
            if (jt == Kinect.JointType.WristLeft || jt == Kinect.JointType.WristRight)
            {
                jointObj.GetComponent<BoxCollider>().isTrigger = true;
                jointObj.AddComponent<TriggerDetector>();
            }

            LineRenderer lr = jointObj.AddComponent<LineRenderer>();
            lr.positionCount = 2;
            lr.material = boneMaterial;
            lr.startWidth = 0.05f;
            lr.endWidth = 0.05f;

            jointObj.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
            jointObj.name = jt.ToString();
            jointObj.transform.position = new Vector3(data.jointsPositionsX[(int)jt], data.jointsPositionsY[(int)jt], data.jointsPositionsZ[(int)jt]);
            jointObj.transform.parent = body.transform;

            // Add to list
            jointObjs.Add(jointObj);
        }

        // Connect the joints with LineRenderer
        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            // Skip if dictionary not contains the joint or other these joints
            if (!_BoneMap.ContainsKey(jt) || jt == Kinect.JointType.Neck
                || jt == Kinect.JointType.ThumbLeft || jt == Kinect.JointType.ThumbRight
                    || jt == Kinect.JointType.HandLeft || jt == Kinect.JointType.HandRight
                    || jt == Kinect.JointType.HandTipLeft || jt == Kinect.JointType.HandTipRight)
                continue;

            Transform jointObj = body.transform.Find(jt.ToString());
            Transform targetJoint = body.transform.Find(_BoneMap[jt].ToString());
            LineRenderer lr = jointObj.GetComponent<LineRenderer>();

            lr.SetPosition(0, jointObj.localPosition);
            lr.SetPosition(1, targetJoint.localPosition);
        }

        beginUpdate = true;
    }
}
