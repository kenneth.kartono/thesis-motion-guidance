using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class JointsData
{
    public List<float> jointsPositionsX = new List<float>();
    public List<float> jointsPositionsY = new List<float>();
    public List<float> jointsPositionsZ = new List<float>();

    public JointsData(Vector3[] jointsData)
    {
        foreach(Vector3 t in jointsData)
        {
            jointsPositionsX.Add(t.x);
            jointsPositionsY.Add(t.y);
            jointsPositionsZ.Add(t.z);
        }
    }
}
