using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDetector : MonoBehaviour
{
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("WristLeft"))
        {
            other.GetComponent<Renderer>().material.color = new Color(1, 1, 1);
            other.GetComponent<LineRenderer>().startColor = new Color(1, 1, 1);
            BodySourceView bsv = other.GetComponentInParent<BodySourceView>();
            bsv.wristLeftLate = false;
            Visualizer_FadeIn vfi = GetComponentInParent<Visualizer_FadeIn>();
            vfi.wristLeftTriggered = true;

            Destroy(gameObject.transform.parent);

        } else if (other.CompareTag("WristRight"))
        {
            other.GetComponent<Renderer>().material.color = new Color(1, 1, 1);
            other.GetComponent<LineRenderer>().startColor = new Color(1, 1, 1);
            BodySourceView bsv = other.GetComponentInParent<BodySourceView>();
            bsv.wristRightLate = false;
            Visualizer_FadeIn vfi = GetComponentInParent<Visualizer_FadeIn>();
            vfi.wristRightTriggered = true;

            Destroy(gameObject.transform.parent);
        }
    }
}
