using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kinect = Windows.Kinect;

public class Visualizer_FadeInSeries : MonoBehaviour
{
    public Material boneMaterial;
    public Material transparentMat;
    public BodySourceView bsv;

    private List<JointsData> jointsDataSeries = new List<JointsData>();
    private List<float> recordingTimesSeries = new List<float>();
    private Dictionary<Kinect.JointType, Kinect.JointType> _BoneMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
    {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },

        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },

        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
        { Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.WristRight },
        { Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
    };

    private float t;
    private bool beginUpdate;
    private List<GameObject> bodies = new List<GameObject>();

    void Update()
    {
        if (!beginUpdate)
            return;

        if (t >= 1)
        {
            Destroy(gameObject);
            return;
        }

        t += Time.deltaTime / 8;
    }

    public void SetData(List<JointsData> jointsDataSeries, List<float> recordingTimesSeries)
    {
        this.jointsDataSeries = jointsDataSeries;
        this.recordingTimesSeries = recordingTimesSeries;
    }

    public void ShowBody()
    {
        for (int i = 0; i < jointsDataSeries.Count; i++)
        {
            // Create GameObject body
            GameObject body = new GameObject("Recorded Body (Visualizer_FadeInSeries) " + i);
            body.transform.parent = gameObject.transform;
            bodies.Add(body);

            for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
            {
                // Skip these joints
                if (jt == Kinect.JointType.Head || jt == Kinect.JointType.ThumbLeft || jt == Kinect.JointType.ThumbRight
                        || jt == Kinect.JointType.HandLeft || jt == Kinect.JointType.HandRight
                        || jt == Kinect.JointType.HandTipLeft || jt == Kinect.JointType.HandTipRight)
                    continue;

                // Create GameObject cubes for joints
                GameObject jointObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
                jointObj.GetComponent<MeshRenderer>().material = transparentMat;

                // Give BoxCollider.isTrigger true and TriggerDetector to WristLeft and WristRight
                if (jt == Kinect.JointType.WristLeft || jt == Kinect.JointType.WristRight)
                {
                    jointObj.GetComponent<BoxCollider>().isTrigger = true;
                    jointObj.AddComponent<TriggerDetectorSeries>();
                }

                LineRenderer lr = jointObj.AddComponent<LineRenderer>();
                lr.positionCount = 2;
                lr.material = boneMaterial;
                lr.startWidth = 0.05f;
                lr.endWidth = 0.05f;

                jointObj.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
                jointObj.name = jt.ToString();
                jointObj.transform.position = new Vector3(jointsDataSeries[i].jointsPositionsX[(int)jt], jointsDataSeries[i].jointsPositionsY[(int)jt], jointsDataSeries[i].jointsPositionsZ[(int)jt]);
                jointObj.transform.parent = body.transform;

                // Don't show joint if current and previous are almost the same position
                if (i > 0)
                {
                    Vector3 prev = new Vector3(jointsDataSeries[i - 1].jointsPositionsX[(int)jt], jointsDataSeries[i - 1].jointsPositionsY[(int)jt], jointsDataSeries[i - 1].jointsPositionsZ[(int)jt]);
                    float distance = (jointObj.transform.position - prev).magnitude;
                    if (distance < 0.05f)
                    {
                        jointObj.SetActive(false);
                    }
                }

                // Set alpha depends on i
                Color newColor = new Color(0.8f, 1, 0, Mathf.Lerp(0, 1, (float)(i + 1) / jointsDataSeries.Count));
                jointObj.GetComponent<MeshRenderer>().material.color = newColor;
                jointObj.GetComponent<LineRenderer>().startColor = newColor;
                jointObj.GetComponent<LineRenderer>().endColor = newColor;

                // Remove LineRenderer component from neck
                if (jt == Kinect.JointType.Neck)
                {
                    Destroy(jointObj.GetComponent<LineRenderer>());
                }
            }

            // Connect the joints with LineRenderer
            for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
            {
                // Skip if dictionary not contains the joint or other these joints
                if (!_BoneMap.ContainsKey(jt) || jt == Kinect.JointType.Neck
                    || jt == Kinect.JointType.ThumbLeft || jt == Kinect.JointType.ThumbRight
                        || jt == Kinect.JointType.HandLeft || jt == Kinect.JointType.HandRight
                        || jt == Kinect.JointType.HandTipLeft || jt == Kinect.JointType.HandTipRight)
                    continue;

                Transform jointObj = body.transform.Find(jt.ToString());
                Transform targetJoint = body.transform.Find(_BoneMap[jt].ToString());
                LineRenderer lr = jointObj.GetComponent<LineRenderer>();

                lr.SetPosition(0, jointObj.localPosition);
                lr.SetPosition(1, targetJoint.localPosition);
            }
        }

        beginUpdate = true;
    }

    private void OnDestroy()
    {
        foreach (GameObject go in bodies)
        {
            Destroy(go);
        }
    }
}
