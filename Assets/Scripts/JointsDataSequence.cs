using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class JointsDataSequence
{
    public List<JointsData> jointsDataSequence = new List<JointsData>();
    public List<float> recordingTimes = new List<float>();

    public JointsDataSequence(List<Vector3[]> jointsSequence, List<float> recordingTimes)
    {
        foreach(Vector3[] vectors in jointsSequence)
        {
            JointsData jointsData = new JointsData(vectors);
            jointsDataSequence.Add(jointsData);
        }

        foreach(float f in recordingTimes)
        {
            this.recordingTimes.Add(f);
        }
    }
}
