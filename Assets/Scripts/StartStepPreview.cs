using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kinect = Windows.Kinect;

public class StartStepPreview : MonoBehaviour
{
    public Material boneMaterial;
    public Material transparentMat;

    private JointsData jointsData;
    private GameObject body;

    private Dictionary<Kinect.JointType, Kinect.JointType> _BoneMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
    {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },

        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },

        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
        { Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.WristRight },
        { Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
    };

    public void SetData(JointsData jointsData)
    {
        this.jointsData = jointsData;
    }

    public void ShowBody()
    {
        // Create GameObject body
        body = new GameObject("StartStepPreview body");
        body.transform.parent = gameObject.transform;

        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            // Skip these joints
            if (jt == Kinect.JointType.Head || jt == Kinect.JointType.ThumbLeft || jt == Kinect.JointType.ThumbRight
                    || jt == Kinect.JointType.HandLeft || jt == Kinect.JointType.HandRight
                    || jt == Kinect.JointType.HandTipLeft || jt == Kinect.JointType.HandTipRight)
                continue;

            // Create GameObject cubes for joints
            GameObject jointObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
            jointObj.GetComponent<MeshRenderer>().material = transparentMat;

            LineRenderer lr = jointObj.AddComponent<LineRenderer>();
            lr.positionCount = 2;
            lr.material = boneMaterial;
            lr.startWidth = 0.05f;
            lr.endWidth = 0.05f;

            jointObj.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
            jointObj.name = jt.ToString();
            jointObj.transform.position = new Vector3(jointsData.jointsPositionsX[(int)jt], jointsData.jointsPositionsY[(int)jt], jointsData.jointsPositionsZ[(int)jt]);
            jointObj.transform.parent = body.transform;

            // Set alpha
            Color newColor = new Color(0.5f, 0.5f, 0.5f, 0.5f);
            jointObj.GetComponent<MeshRenderer>().material.color = newColor;
            jointObj.GetComponent<LineRenderer>().startColor = newColor;
            jointObj.GetComponent<LineRenderer>().endColor = newColor;

            // Remove LineRenderer component from neck
            if (jt == Kinect.JointType.Neck)
            {
                Destroy(jointObj.GetComponent<LineRenderer>());
            }
        }

        // Connect the joints with LineRenderer
        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            // Skip if dictionary not contains the joint or other these joints
            if (!_BoneMap.ContainsKey(jt) || jt == Kinect.JointType.Neck
                || jt == Kinect.JointType.ThumbLeft || jt == Kinect.JointType.ThumbRight
                    || jt == Kinect.JointType.HandLeft || jt == Kinect.JointType.HandRight
                    || jt == Kinect.JointType.HandTipLeft || jt == Kinect.JointType.HandTipRight)
                continue;

            Transform jointObj = body.transform.Find(jt.ToString());
            Transform targetJoint = body.transform.Find(_BoneMap[jt].ToString());
            LineRenderer lr = jointObj.GetComponent<LineRenderer>();

            lr.SetPosition(0, jointObj.localPosition);
            lr.SetPosition(1, targetJoint.localPosition);
        }
    }

    public void DeleteBody()
    {
        Destroy(body);
    }
}
