using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
public delegate void FPtr(int value);

public class Haptics : MonoBehaviour
{
    private int interfaceId;

    class EAIWrapper
    {
        [DllImport("TactorInterface.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int InitializeTI();

        [DllImport("TactorInterface.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Discover(int type);

        [DllImport("TactorInterface.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Connect([MarshalAs(UnmanagedType.LPStr)] string name, int type, FPtr _callback);

        [DllImport("TactorInterface.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Pulse(int deviceId, int _tacNum, int _msDuration, int _delay);

        [DllImport("TactorInterface.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int ChangeFreq(int deviceId, int _tacNum, int freqVal, int _delay);

        [DllImport("TactorInterface.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int ChangeGain(int deviceID, int _tacNum, int gainval, int _delay);

        [DllImport("TactorInterface.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Close(int interfaceId);

        [DllImport("TactorInterface.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int ShutdownTI();
    }

    void Start()
    {
        EAIWrapper.InitializeTI();
        EAIWrapper.Discover(1);
        interfaceId = EAIWrapper.Connect("COM5", 1, null);
    }

    /// <summary>
    /// Vibrate a haptic motor
    /// </summary>
    /// <param name="motor">Motor: 1-16</param>
    /// <param name="intensity">Intensity range: 0.0-1.0</param>
    /// <param name="vibrationTime">Vibration time in miliseconds</param>
    public void Vibrate(int motor, double intensity, int vibrationTime)
    {
        EAIWrapper.ChangeGain(interfaceId, motor, (int)(intensity * 255), 0);
        EAIWrapper.Pulse(interfaceId, motor, vibrationTime, 0);
        EAIWrapper.ChangeFreq(interfaceId, motor, 2000, 0);
    }

    private void OnDestroy()
    {
        EAIWrapper.Close(interfaceId);
        EAIWrapper.ShutdownTI();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            Vibrate(15, 1, 100);
        }
    }
}
