using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class CSVWriter
{
    private static string path = Application.dataPath + "/data";

    public static void WriteCSV(List<List<Vector3>> posJointsDemos, List<List<Vector3>> posJointsBodys, List<float> csvTimes, ModeController mc)
    {
        string[] jointsName = { "SpineBase", "SpineMid", "Neck", "ShoulderLeft", "ElbowLeft", "WristLeft", "ShoulderRight",
        "ElbowRight", "WristRight", "HipLeft", "KneeLeft", "AnkleLeft", "FootLeft", "HipRight", "KneeRight", "AnkleRight",
        "FootRight", "SpineShoulder" };

        string demos = "";
        string bodys = "";

        foreach (string name in jointsName)
        {
            demos += "Demo_" + name + ",";
            bodys += "Body_" + name + ",";
        }

        path += "_" + mc.perspective.ToString() + "_" + mc.feedback.ToString() + ".csv";
        StreamWriter sw = new StreamWriter(path);
        sw.WriteLine(demos + bodys + "time");

        for (int i = 0; i < posJointsDemos.Count; i++)
        {
            List<Vector3> demo = posJointsDemos[i];
            List<Vector3> body = posJointsBodys[i];

            string d = "";
            string b = "";

            for (int j = 0; j < demo.Count; j++)
            {
                d += Vector3ToString(demo[j]) + ",";
                b += Vector3ToString(body[j]) + ",";
            }
            sw.WriteLine(d + b + csvTimes[i]);
        }
        sw.Close();
    }

    private static string Vector3ToString(Vector3 vector)
    {
        return vector.x.ToString() + ";" + vector.y.ToString() + ";" + vector.z.ToString();
    }
}
