using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class SaveSystem
{
    private static readonly string path = Application.dataPath + "/joints.sav";

    public static void Save(List<Vector3[]> jointsSequence, List<float> recordingTimes)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = new FileStream(path, FileMode.Create);

        JointsDataSequence data = new JointsDataSequence(jointsSequence, recordingTimes);
        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static JointsDataSequence Load()
    {
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            JointsDataSequence data = formatter.Deserialize(stream) as JointsDataSequence;
            stream.Close();
            return data;
        } else
        {
            Debug.LogError("Save file not found in " + path);
            return null;
        }
    }
}
