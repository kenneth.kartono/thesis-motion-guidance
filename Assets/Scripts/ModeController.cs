using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeController : MonoBehaviour
{
    public enum Perspective
    {
        FirstPersonPerspective,
        ThirdPersonPerspective,
        ThirdPersonPerspectiveWithMultipleViews
    };

    public enum Feedback
    {
        NoFeedback,
        ColorFeedback,
        HapticFeedback
    }

    public Perspective perspective;
    public Feedback feedback;

    public GameObject monitors;

    private void Start()
    {
        if (perspective == Perspective.ThirdPersonPerspectiveWithMultipleViews)
        {
            monitors.SetActive(true);
        }
        else
        {
            monitors.SetActive(false);
        }
    }
}
