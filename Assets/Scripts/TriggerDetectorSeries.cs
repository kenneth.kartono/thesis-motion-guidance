using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDetectorSeries : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("WristLeft"))
        {
            // Destroy visualization step if it is touched
            Destroy(gameObject.transform.parent.gameObject);
        }
        else if (other.CompareTag("WristRight"))
        {
            Destroy(gameObject.transform.parent.gameObject);
        }
    }
}
